pragma solidity >=0.4.22 <0.7.0;
pragma experimental ABIEncoderV2;
import "./IniadToken.sol";

/*
クラスコントラクト
※クラス構造体
※契約間では構造体を渡すことができないので、内部で渡す関数を用意
*/
contract Class{
    
    struct classStruct{
        uint classNum; //クラス番号
        string teacherName; //教員名
        address[] adds; //生徒アドレス
    }
    
    classStruct[] public classArray;
    
    function GetOneClassNum(uint n) constant public returns(uint){
        return(classArray[n].classNum);
    }
    
    function GetOneClassAdds(uint n) constant public returns(address[]){
        return(classArray[n].adds);
    }
}

/*
管理コントラクト
※クラスコントラクトを継承
※クラスの生成、生徒の登録
*/
contract Distribution is Class , TokenERC20{

    function CreatClass(uint _num, string _name, address[] _adds){
        classStruct memory newClass = classStruct({
           classNum:_num,
           teacherName: _name,
           adds:_adds
        });
        classArray.push(newClass);
    }
    
    function Registration(address _add, uint _num){
        uint counter=classArray.length;
        for(uint i=0;i<counter;i++){
            if(classArray[i].classNum==_num){
                classArray[i].adds.push(_add);
                break;
            }
        }
    }
    
    //function DeleteClass(){}
    //function DelecteAddress(){}


/*
トークン配布コントラクト
※クラスコントラクト、TokenERC20コントラクトを継承
*/
    
    function Test(uint _num) returns(uint){
        return(GetOneClassNum(_num));
    }

    //評価関数
    function EvalAvg(uint[] _nums, uint[] _avgScores) constant public returns(address[]){
        uint maxAvg=_avgScores[0];
        uint index=0;
        for(uint i=1;i<_avgScores.length;i++){
            if(maxAvg < _avgScores[i]){
                maxAvg = _avgScores[i];
                index=i;
            }
        }
        uint topClass = _nums[index];
        
        for(uint j=0;j<topClass;j++){
            if(GetOneClassNum(j)==topClass){
                return GetOneClassAdds(j);
            }
        }
    }

    //配布関数
    function Dist(uint[] _nums, uint[] _avgScores){
        address[] memory array = EvalAvg(_nums, _avgScores);
        uint counter = array.length;
        
        for(uint i=0;i<counter;i++){
            TokenERC20.transfer(array[i], 100);
        }
    }
}
